//
//  BIDActivityListView.m
//  zhongrongbao
//
//  Created by mal on 15/7/13.
//  Copyright (c) 2015年 cnsoft. All rights reserved.
//

#import "BIDActivityListView.h"

@interface BIDActivityListView()<UITableViewDataSource, UITableViewDelegate>
{
    UITableView *_tableView;
    NSMutableArray *_dataSourceArr;
}
@end

@implementation BIDActivityListView
@synthesize delegate;
@synthesize activityType;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        _dataSourceArr = [[NSMutableArray alloc] init];
        _tableView = [[UITableView alloc] initWithFrame:self.bounds];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        [_tableView setBackgroundColor:[UIColor colorWithRed:244.0f/255.0f green:244.0f/255.0f blue:244.0f/255.0f alpha:1.0f]];
        //_tableView.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:_tableView];
//        NSLayoutConstraint *topConstraint = [NSLayoutConstraint constraintWithItem:_tableView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1.f constant:0.f];
//        NSLayoutConstraint *bottomConstraint = [NSLayoutConstraint constraintWithItem:_tableView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1.f constant:0.f];
//        NSLayoutConstraint *trailingConstraint = [NSLayoutConstraint constraintWithItem:_tableView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTrailing multiplier:0.f constant:0.f];
//        NSLayoutConstraint *leadingConstraint = [NSLayoutConstraint constraintWithItem:_tableView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeading multiplier:1.f constant:0.f];
//        [self addConstraints:@[topConstraint, bottomConstraint, trailingConstraint, leadingConstraint]];
    }
    return self;
}

- (void)setDataSourceArr:(NSArray *)arr
{
    [_dataSourceArr setArray:arr];
    [_tableView reloadData];
}

- (void)editSubViewWidth:(CGFloat)width
{
    CGRect frame = _tableView.frame;
    frame.size.width = width;
    _tableView.frame = frame;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataSourceArr.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    static NSString *identifier = @"identifier";
    NSUInteger row = indexPath.row;
    cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    if(row==0)
    {
        cell.textLabel.text = _dataSourceArr[0];
    }
    else
    {
        NSDictionary *dic = _dataSourceArr[row];
        cell.textLabel.text = [[NSString alloc] initWithFormat:@"%@元", dic[@"amt"]];
    }
    cell.textLabel.font = [UIFont systemFontOfSize:15.0f];
    [cell setBackgroundColor:[UIColor colorWithRed:244.0f/255.0f green:244.0f/255.0f blue:244.0f/255.0f alpha:1.0f]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self removeFromSuperview];
    NSUInteger row = indexPath.row;
    NSString *strActName = @"";
    NSString *strActId = @"";
    if(row==0)
    {
        strActName = _dataSourceArr[0];
        strActId = @"";
    }
    else
    {
        NSDictionary *dic = _dataSourceArr[row];
        strActName = [[NSString alloc] initWithFormat:@"%@元", dic[@"amt"]];
        strActId = dic[@"aid"];
    }
    switch(activityType)
    {
        case ACTIVITY_REDPACKET:
        {
            [delegate selectActivityForRedPacketWithName:strActName activityId:strActId];
        }
            break;
        case ACTIVITY_TIYANJIN:
        {
            [delegate selectActivityForTiYanJinWithName:strActName activityId:strActId];
        }
            break;
    }
}

@end
