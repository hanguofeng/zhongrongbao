//
//  BIDEditMobilePhoneNumberFirstStepViewController.h
//  zhongrongbao
//
//  Created by mal on 15/6/28.
//  Copyright (c) 2015年 cnsoft. All rights reserved.
//

#import "BIDBaseViewController.h"
@class BIDCustomTextField;

@interface BIDEditMobilePhoneNumberFirstStepViewController : BIDBaseViewController
{
    IBOutlet UITextField *_mobilePhoneNumberTF;
    //图片验证码
    IBOutlet BIDCustomTextField *_imgCodeTF;
    IBOutlet UIImageView *_authenticationCodeImgView;
}

/**
 *原绑定手机号码
 */
@property (copy, nonatomic) NSString *oldBindingMobilePhoneNumber;

@end
